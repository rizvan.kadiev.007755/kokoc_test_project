from django.views.generic import TemplateView
from django.forms import Form, DateField
from datetime import datetime
from .models import CurrencyRate


class DateForm(Form):
    date = DateField(input_formats=['%Y-%m-%d'])


class ShowRatesView(TemplateView):
    template_name = 'exchange/show_rates.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        date_form = DateForm(self.request.GET)

        if date_form.is_valid():
            date = date_form.cleaned_data['date']
            context.update(self.get_rates_context(date))
        else:
            context['error'] = "Неверный формат даты или дата не указана. Пожалуйста, используйте формат ГГГГ-ММ-ДД."

        return context

    def get_rates_context(self, date):
        rates = CurrencyRate.objects.filter(date=date)
        if rates.exists():
            return {'rates': rates}
        else:
            return {'error': "На эту дату нет курса. Пожалуйста, выберите другую дату."}
