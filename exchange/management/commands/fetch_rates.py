import requests
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from exchange.models import Currency, CurrencyRate
from datetime import datetime
import logging

logger = logging.getLogger('django')


class Command(BaseCommand):
    help = 'Получает курсы валют и сохраняет их в базе данных.'

    def handle(self, *args, **options):
        try:
            response = requests.get(settings.CBR_API_URL)
            response.raise_for_status()
            data = response.json()
            currency_val = data['Valute']
            with transaction.atomic():
                for code, val in currency_val.items():
                    currency, _ = Currency.objects.get_or_create(
                        char_code=val['CharCode'],
                        defaults={'name': val['Name']}
                    )
                    CurrencyRate.objects.update_or_create(
                        currency=currency,
                        date=datetime.now().date(),
                        defaults={'value': val['Value']}
                    )
                logger.info("Данные сохранены в базе. Успешно!")
        except requests.RequestException as e:
            logger.error(f"Ошибка получения данных API: {e}")
        except KeyError as e:
            logger.error(f"Отсутствуют данные в ответе: {e}")
