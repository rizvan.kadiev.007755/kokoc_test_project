from django.test import TestCase, Client
from django.urls import reverse
from .models import Currency, CurrencyRate
from datetime import datetime


class ShowRatesViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse('show_rates')
        CurrencyRate.objects.create(currency=Currency.objects.create(char_code='USD', name='Доллар США'),
                                    date=datetime.now().date(), value=75.00)

    def test_show_rates_valid_date(self):
        response = self.client.get(self.url, {'date': datetime.now().date().strftime('%Y-%m-%d')})
        self.assertEqual(response.status_code, 200)
        self.assertIn('75.00', response.content.decode())

    def test_show_rates_invalid_date(self):
        response = self.client.get(self.url, {'date': 'invalid-date'})
        self.assertEqual(response.status_code, 200)
        self.assertIn('Неверный формат даты или дата не указана.', response.content.decode())

    def test_show_rates_no_date(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('Неверный формат даты или дата не указана.', response.content.decode())

    def test_show_rates_no_rates_available(self):
        response = self.client.get(self.url, {'date': '2024-12-31'})
        self.assertEqual(response.status_code, 200)
        self.assertIn('На эту дату нет курса.', response.content.decode())
