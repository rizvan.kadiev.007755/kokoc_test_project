.PHONY: build restart test migrate makemigrations command shell

build:
	docker-compose up --build
restart:
	docker-compose restart
migrate:
	docker-compose exec django python manage.py migrate
makemigrations:
	docker-compose exec django python manage.py makemigrations
superuser:
	docker-compose exec django python manage.py createsuperuser
command:
	docker-compose exec django python manage.py fetch_rates
test:
	docker-compose exec django python manage.py test
shell:
	docker-compose exec django python manage.py shell
